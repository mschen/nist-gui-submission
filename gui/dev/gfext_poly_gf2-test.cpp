
#include <stdio.h>

#include <stdlib.h>

#include "blas.h"

#include "gfext_poly_gf2.h"

#include <openssl/rand.h>
#include "prng_utils.h"



#include "gui_config.h"

#define _GFSIZE  GUI_BGF_SIZE
#define _DEG  GUI_C_DEG
#define _MAX_TERMS (((_DEG+7)/8)*8)
#define _GF_EXT_ _GFSIZE
#include "gfext.h"
#define _TERMS (_DEG+1)
#define _DEG_1 (_DEG-1)


#define EXT _GFSIZE

//#define _DEG (9)
//#define _TERMS (10)

#define MUL BGFMUL
#define SQU BGFSQU
#define INV BGFINV

uint8_t sol[EXT] __attribute__((aligned(16)));

int main()
{
	unsigned char seed[32];
	RAND_bytes( seed , 32 );
	prng_seed( seed , 32 );

	uint8_t pa[EXT*_TERMS] __attribute__((aligned(16))) = {0};
	uint8_t pb[EXT*_TERMS] __attribute__((aligned(16))) = {0};
	uint8_t pc[EXT*_TERMS] __attribute__((aligned(16))) = {0};
	uint8_t pd[EXT*_TERMS] __attribute__((aligned(16))) = {0};
	uint8_t pe[EXT*_TERMS] __attribute__((aligned(16))) = {0};
	uint8_t pf[EXT*_TERMS] __attribute__((aligned(16))) = {0};

	uint8_t ttt[EXT] __attribute__((aligned(16))) ;
	gf256v_rand(ttt,EXT-1);
	gf256v_rand(pa,EXT);
	memcpy( sol,pa,EXT);
	//pa[0] = 0x2;
	pa[EXT] = 1;

	gf256v_rand(pb,_TERMS*EXT);
	/// x17 + x^16 + x8 + x5 + x4 + x2 + x + 1
	memset( pb + 7*EXT , 0 , EXT );
	gf256v_rand(pc,_TERMS*EXT);

	printf("pa :"); poly_fdump(stdout,pa,2); printf("\n");
	//printf("pb :"); poly_fdump(stdout,pb,3); printf("\n");
	//printf("pc :"); poly_fdump(stdout,pc,3); printf("\n");

	printf("\n\n================ deg 2 GCD test! ================\n\n");
	//poly_muladd(pd,pb,16,0,pa);
	//printf("pd :"); poly_fdump(stdout,pd,3); printf("\n");
	//poly_muladd(pd,pb,16,1,pa+EXT);

	poly_mul_2( pf , pb , 1 , pc , 1 );
	poly_eval( ttt , pf , 2 , pa );
	gf256v_add( pf , ttt ,EXT );
	poly_mul_2( pd , pf , 2 , pb , _DEG-2 );
	poly_mul_2( pe , pf , 2 , pc , _DEG-2 );
	printf("pf :"); poly_fdump(stdout,pf,2); printf("\n");
	memset( pf , 0 , EXT*_TERMS );

	unsigned _r = _get_gcd( pf , pd , pe , _DEG );
	printf("\n\nr=%d, GCD: \n", _r);
	poly_fdump(stdout,pf,_r+1); printf("\n");
	poly_eval( ttt , pf , _r , pa );
	printf(" = 0? %d\n", gf256v_is_zero(ttt,EXT));
	printf("\n\n");
	//return 0;


	printf("\n\n================ GCD test! ================\n\n");
	//poly_muladd(pd,pb,16,0,pa);
	//printf("pd :"); poly_fdump(stdout,pd,3); printf("\n");
	//poly_muladd(pd,pb,16,1,pa+EXT);
	memcpy( pd, pb , _TERMS*EXT );
	poly_eval( ttt , pb , _DEG , pa );
	gf256v_add( pd , ttt ,EXT );
	printf("pd :"); poly_fdump(stdout,pd,_DEG); printf("\n");

	memcpy( pe, pc , _TERMS*EXT );
	poly_eval( ttt , pc , _DEG , pa );
	gf256v_add( pe , ttt ,EXT );
	printf("pe :"); poly_fdump(stdout,pe,_DEG); printf("\n");

	unsigned r = _get_deg1poly_gcd( pf , pd , pe , _DEG );
	//unsigned r = _get_gcd( pf , pd , pe , _DEG );
	printf("\n\nr=%d, GCD: \n", r);
	poly_fdump(stdout,pf,r+1); printf("\n");
	poly_eval( ttt , pf , r , pa );
	printf(" = 0? %d\n", gf256v_is_zero(ttt,EXT));
	printf("\n\n");



	printf("\n\n================ find unique root test! ================\n\n");
	uint8_t _pd[EXT*_TERMS] __attribute__((aligned(16))) = {0};
	/// x17 + x^16 + x8 + x5 + x4 + x2 + x + 1
	//memcpy( _pd , pd , EXT*_TERMS );
	//memcpy( _pd+7*EXT , pd+8*EXT , 2*EXT );
	poly_normalize( _pd , pd , _DEG );
	unsigned degree[_TERMS];
	for(unsigned i=0;i<_TERMS;i++) degree[i]=i;

	printf("_pd :"); poly_fdump(stdout,_pd,_DEG); printf("\n");
	//r = find_unique_root( pf , _pd );
	//r = find_unique_root_sparse_poly( pf , _pd , degree , _TERMS );
	r = find_unique_root_sparse_poly( pf , _pd , degree , _TERMS );
	printf("r = %d, root: ",r ); gf256v_fdump(stdout,pf,EXT); printf("\n");
	printf("pa :"); poly_fdump(stdout,pa,0); printf("\n");
	gf256v_add( pf , pa , EXT );
	printf("(have to try multiple times) eq?: %d\n", gf256v_is_zero(pf,EXT) );


	printf("\n\n================ find random root test! ================\n\n");

	r = find_random_root_sparse_poly( pf , _pd , degree , _TERMS );
	printf("r = %d, gcd: ",r ); gf256v_fdump(stdout,pf,EXT); printf("\n");
	//poly_fdump(stdout,pf,r); printf("\n");
	//memset( pf + EXT , 0 , EXT );
	//pf[EXT] = 1;
	//poly_eval( ttt , pf , 1 , pa );
	poly_eval( ttt , _pd , _DEG , pf );
	printf(" = 0? %d\n", gf256v_is_zero(ttt,EXT));
	printf("\n\n");

	return 0;
}
